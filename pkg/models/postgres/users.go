package postgres
import (
	"context"
	"cs01.com/snippetbox/pkg/models"
	"database/sql"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	insertUsSql = "INSERT INTO users (name,email,hashed_password,created) VALUES ($1,$2,$3,$4) RETURNING id"
	authenticate = "SELECT id, hashed_password FROM users WHERE email = $1 AND active = TRUE"
    get =  "SELECT id, name, email, created, active FROM users WHERE id = $1"
)

type UserModel struct {
	Pool *pgxpool.Pool
}

// We'll use the Insert method to add a new record to the users table.
func (m *UserModel) Insert(name, email, password string)  error {

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}

	var id uint64
	row := m.Pool.QueryRow(context.Background(),insertUsSql,name, email,hashedPassword,time.Now())
	err = row.Scan(&id)
	if err != nil {
		// If this returns an error, we use the errors.As() function to check
		// whether the error has the type *mysql.MySQLError. If it does, the
		// error will be assigned to the mySQLError variable. We can then check
		// whether or not the error relates to our users_uc_email key by
		// checking the contents of the message string. If it does, we return
		// an ErrDuplicateEmail error.

		err := row.Scan(&hashedPassword)
		if err != nil {
			return  err
		}
		return err
	}
	return nil
}

// We'll use the Authenticate method to verify whether a user exists with
// the provided email address and password. This will return the relevant
// user ID if they do.
func (m *UserModel) Authenticate(email, password string) (int, error) {
	// Retrieve the id and hashed password associated with the given email. If no
	// matching email exists, or the user is not active, we return the
	// ErrInvalidCredentials error.
	var id int
	var hashedPassword []byte
	row := m.Pool.QueryRow(context.Background(),authenticate, email)
	err := row.Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}
	// Check whether the hashed password and plain-text password provided match.
	// If they don't, we return the ErrInvalidCredentials error.
	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}
	// Otherwise, the password is correct. Return the user ID.
	return id, nil
}


// We'll use the Get method to fetch details for a specific user based
// on their user ID.
func (m *UserModel) Get(id int) (*models.User, error) {
	u := &models.User{}

	err := m.Pool.QueryRow(context.Background(), get, id).
		Scan(&u.ID, &u.Name, &u.Email, &u.Created, &u.Active)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return u, nil
}


